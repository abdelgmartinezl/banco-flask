from flask import Flask, redirect, url_for, render_template, request, session, flash
from flask_mail import Mail, Message
from form import ContactoForm


app = Flask(__name__)
app.secret_key = 'AjqXMpOu4T'
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = '465' 
app.config['MAIL_USERNAME'] = 'fenixflamada@gmail.com'
app.config['MAIL_PASSWORD'] = 'mauricio1998'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


@app.route('/')
def iniciar():
    if 'nombre' in session:
        return redirect(url_for('arrancar'))
    return render_template('index.html')


@app.route('/login/')
def login():
    return render_template('login.html')


@app.route('/logout', methods=['POST', 'GET'])
def logout():
    if request.method == 'POST':
        session.pop('nombre', None)
        return redirect(url_for('iniciar'))
    elif request.method == 'GET':
        return '<h1>Asi te queria agarrar puerco!</h1>'


@app.route('/landing', methods=['POST', 'GET'])
def arrancar():
    if request.method == 'GET':
        if 'nombre' in session:
            nom = session['nombre']
            return render_template('landing.html', nombre=nom)
        return redirect(url_for('iniciar'))
    elif request.method == 'POST':
        nom = request.form['nm']
        session['nombre'] = nom
        return render_template('landing.html', nombre=nom)
    else:
        return 'Metodo no valido'


@app.route('/itbms')
def calcular_itbms():
    return render_template('itbms.html')


@app.route('/itbms/resultado', methods=['POST'])
def mostrar_itbms():
    try: 
        valor = float(request.form['valor'])
        resultado = valor * 0.07
    except:
        valor = -1.0
        resultado = valor
        flash('La proxima vez coloque un valor numerico')
    mensaje = Message('Banco Te Robo - ITBMS', sender='fenixflamada@gmail.com', recipients=['diegomsc17@gmail.com'])
    mensaje.body = 'El ITBMS de ' + str(valor) + " es igual a $" + str(resultado)
    mail.send(mensaje)
    return render_template('resultado_itbms.html', val=valor, res=resultado)


@app.route('/interes/<float:saldo>')
def calcular_interes(saldo):
    interes = saldo * 0.05
    return 'Si tuvieras un saldo de $%f, tu interes seria de $%f' % (saldo, interes)


@app.route('/<nombre>/<int:edad>/')
def ejemplo(edad, nombre):
    if edad >= 18:
        return 'Saludos Sr(a). %s' % nombre
    else:
        return 'Disculpe, llame a su papa/mama'


@app.route('/contacto')
def contacto():
    form = ContactoForm()
    return render_template('contacto.html', form=form)


@app.route('/notificacion', methods=['POST'])
def notificar():
    if request.method == 'POST':
        name = request.form['nombre']
        email = request.form['email']
        edad = request.form['edad']
        sector = request.form['sector']
        mensaje = Message('Banco Te Robo - Contacto', sender='fenixflamada@gmail.com', recipients=['abdel.martinez@yahoo.com'])
        if int(edad) > 18:
            mensaje.body = "Hola, " + name + ". Te puedo ofrecer una tarjeta."
        else:
            mensaje.body = name + ", llama a tu papa o mama."
        mail.send(mensaje)
        return 'Estaremos en contacto con usted. Debio recibir un e-mail...'
    else:
        return redirect(url_for('iniciar')) 


if __name__ == '__main__':
    app.run()
