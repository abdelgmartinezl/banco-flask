from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, IntegerField, SelectField, SubmitField
from wtforms import validators


class ContactoForm(FlaskForm):
    nombre = TextField("Cliente", [validators.Required("Introduzca nombre...")])
    email = TextAreaField("Correo Electronico", [validators.Required("Introduzca correo..."), validators.Email("Correo invalido...")])
    edad = IntegerField("Edad")
    sector = SelectField('Sector', choices=[('pri', 'Privada'),('gob', 'Gobierno')])
    submit = SubmitField("Enviar")
